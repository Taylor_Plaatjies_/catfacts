<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\CatFact;
use GuzzleHttp\Client;
use Auth;

class CatFactsCrud extends Component
{
    use WithPagination;

    public $fact_text = '';
    public $modalOpen = false;
    public $searchTerm = '';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.catfacts', [
            'catFacts' => CatFact::where('fact_text', 'like', '%'.$this->searchTerm.'%')->paginate(5)
        ]);
    }

    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->modalOpen = true;
    }

    public function closeModal()
    {
        $this->modalOpen = false;
    }

    private function resetInputFields(){
        $this->fact_text = '';
    }

    public function store()
    {
        $this->validate([
            'fact_text' => 'required',
        ]);
        
        $catFact = Catfact::where('fact_text', $this->fact_text)->first();
        if(!$catFact) {
            $catFact = new CatFact();
            $catFact->fact_id = null;
            $catFact->user_id = Auth::user()->id;
            $catFact->fact_text = $this->fact_text;
            $catFact->save();
    
            session()->flash('message', 'Cat Fact successfully created.');
        } else {
            session()->flash('message', 'Cat Fact could not be created. Fact already exists.');
        }
  
        $this->closeModal();
        $this->resetInputFields();
    }

    public function syncCatFacts() {
        $client = new Client(['base_uri' => 'https://cat-fact.herokuapp.com']);

        $response = $client->request('GET', '/facts');

        $results = json_decode($response->getBody()->getContents());
        if(count($results) > 0) {
            foreach($results as $result) {
                $user_id = null;
                if(Auth::user()) {
                    $user_id = Auth::user()->id;
                }

                
                $catFact = CatFact::where('fact_id', $result->_id)->first();
                if(!$catFact) {
                    $catFact = new CatFact();
                    $catFact->fact_id = $result->_id;
                }
                
                $catFact->user_id = $user_id;
                $catFact->fact_text = $result->text;
                $catFact->save();
            }
        }

        session()->flash('message', 'Cat Facts successfully updated.');
    }
}
