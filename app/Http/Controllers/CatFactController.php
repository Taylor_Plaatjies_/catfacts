<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CatFact;
use Illuminate\Support\Facades\Validator;
use Auth;

class CatFactController extends Controller
{
    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'fact_text' => 'required|unique:cat_facts',
        ], [
            'fact_text.unique' => 'Cat Fact already exists.'
        ]);
         
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('message', $validator->messages()->first());
        }

        $catFact = Catfact::where('fact_text', $request->fact_text)->first();
        if(!$catFact) {
            $user = Auth::user();
            $catFact = new CatFact();
            $catFact->fact_id = null;
            $catFact->user_id = $user->id;
            $catFact->fact_text = $request->fact_text;
            $catFact->save();

            $user->submitted_cat_fact = true;
            $user->save();

            return redirect('/')->with('message', 'Cat Fact successfully created.');
        } else {
            return redirect()->back()->withInput()->with('message', 'Could not create Cat Fact. It already exists.');
        }
    }
}
