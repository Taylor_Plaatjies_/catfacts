<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class EnsureUserHasSubmittedCatFact
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::user()->submitted_cat_fact) {
            return redirect('/create')->with('message', 'You need to create a Cat Fact before you can use the View or Sync functionality.');
        }

        return $next($request);
    }
}
