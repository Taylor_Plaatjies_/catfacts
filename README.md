## Set up
* Run npm install && npm run dev
* Run composer install
* Update your .env file with your DB variables.
* Run migration and seeder
* Use test user: ricus@zero2one.co.za password