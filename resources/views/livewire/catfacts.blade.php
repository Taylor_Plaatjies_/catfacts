<div class="py-12">
    <div wire:loading class="fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden bg-gray-700 opacity-75 flex flex-col items-center justify-center">
        <div class="loader ease-linear rounded-full border-4 border-t-4 border-gray-200 h-12 w-12 mb-4"></div>
        <h2 class="text-center text-white text-xl font-semibold">Loading...</h2>
    </div>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            @if (session()->has('message'))
                <div class="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3" role="alert">
                  <div class="flex">
                    <p class="text-sm">{{ session('message') }}</p>
                  </div>
                </div>
            @endif
            <div class="grid grid-cols-3 gap-4">
                <div>
                    <button wire:click="syncCatFacts()" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3">Sync Cat Facts</button>
                </div>
                <div class="px-4 py-2"><input class="w-full" type="text" wire:model="searchTerm" placeholder="Search..." /></div>
                <div>
                    <button wire:click="create()" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3 float-right">Create New Cat Fact</button>
                </div>
            </div>
            @if($modalOpen)
                @include('livewire.create')
            @endif
            <table class="table-fixed w-full">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="px-4 py-2 w-20">ID</th>
                        <th class="px-4 py-2">Text</th>
                        <th class="px-4 py-2">User</th>
                        <th class="px-4 py-2">Updated At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($catFacts as $catFact)
                        <tr>
                            <td class="border px-4 py-2">{{ $catFact->id }}</td>
                            <td class="border px-4 py-2">{{ $catFact->fact_text }}</td>
                            <td class="border px-4 py-2">
                                @if(isset($catFact->user))
                                    {{ $catFact->user->name ?: '-' }}
                                @else
                                    -
                                @endif
                            </td>
                            <td class="border px-4 py-2">{{ $catFact->updated_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        {{ $catFacts->links() }}
    </div>
</div>