<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\CatFact;

class CatFactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seedCatFacts = [
            [
                'fact_text'     => 'This is the first Cat Fact! Cats are mostly portrayed as cold and uncaring. This is not always the case.',
            ],
            [
                'fact_text'     => 'This is the second Cat Fact! This is the text used in the seeder, used to test the search and pagination functionality.',
            ],
        ];
        
        foreach ($seedCatFacts as $seedCatFact) {
            CatFact::create($seedCatFact);
        }
    }
}
