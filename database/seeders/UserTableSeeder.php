<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seedUsers = [
            [
                'name'          => 'Taylor Plaatjies',
                'email'         => 'taylor.plaatjies@gmail.com',
                'password'      => Hash::make('password'),
            ],
            [
                'name'          => 'Ricus Swanepoel',
                'email'         => 'ricus@zero2one.co.za',
                'password'      => Hash::make('password'),
            ],
        ];
        
        foreach ($seedUsers as $seedUser) {
            User::create($seedUser);
        }
    }
}
